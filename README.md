#Apollo Server GraphQL Wrapper for Yelp Fusion API

A GraphQL wrapper for Yelp Fusion API built using Apollo Server.  This is intended to be a POC of an express and HAPI GraphQL server.

Uses:

* [apollo-server](https://github.com/apollostack/apollo-server) - Apollo server GraphQL middleware for express.
* [graphql-js](https://github.com/graphql/graphql-js) - a JavaScript GraphQL runtime.
* [DataLoader](https://github.com/facebook/dataloader) - for coalescing and caching fetches.
* [GraphiQL](https://github.com/graphql/graphiql) - for easy exploration of this GraphQL server.

## Getting Started

Install dependencies with

```sh
npm install
```

## Local Server

A local server is in `./src`. It can be run with:

```sh
npm start
```

This is will start both an express server on port 3000 and a HAPI server on port 8000.

You can explore the API at http://localhost:3000 or http://localhost:8000

## Development Server

A development server that watches for changes can be ran with:

```sh
npm run dev
```
## Sample queries
query yelp($term: String, $location: String, $phone: String, $businessId: String) {

  searchForBusiness(term: $term, location: $location) {
  
    businesses {
    
      ...businessInfo
      
    }
    
  }
  
  phone: phoneSearchForBusiness(phone: $phone) {
  
    total,
    
    businesses {
    
      ...businessInfo
      
    }
    
  }
  
  delivery: transactionSearch(location: $location) {
    total,
    
    businesses {
    
      ...businessInfo
      
    }
    
  }
  
  business: getBusiness(businessId: $businessId) {
  
    id,
    
    name,
    
    image_url,
    
    url,
    
    price,
    
    phone,
    
    rating,
    
    review_count,
    
    photos,
    
    categories {
    
      title
    }
    
    location {
    
      display_address,
      
      cross_streets
      
    }
    
    hours {
      is_open_now,
      
      hours_type,
      
      open {
      
        is_overnight,
        
        day,
        
        start,
        
        end
        
      }
      
    }
    
  }
  
  reviews: getReviews(businessId: $businessId) {
  
    total,
    
    reviews {
    
      text,
      
      url,
      
      rating,
      
      time_created,
      
      user {
      
        name,
        
        image_url
        
      }
      
    }
    
  }
  
  autoComplete(text: "Delivery", longitude: -121.927605271339, latitude: 37.4914844334126) {
  
    terms {
    
      text
      
    }
    
    businesses {
    
      id,
      
      name
      
    }
    
    categories {
    
      title
      
    }
    
  }
  
}

fragment businessInfo on Business {

  id,
  
  name,
  image_url,
  
  url,
  
  price,
  
  phone,
  
  rating,
  
  review_count,
  
  distance,
  
  categories {
  
    title
    
  },
  
  location {
  
    display_address
    
  },
  
  coordinates {
  
    longitude,
    
    latitude
    
  }
  
}

Query variables:

{
  "term": "restaurants",
  
  "location": "94539",
  
  "phone": "+15106518989",
  
  "businessId": "causeway-bay-bistro-yakitori-charcoal-grill-and-ramen-fremont"
  
}