const day = (openhr) => openhr.day
const start = (openhr) => openhr.start
const end = (openhr) => openhr.end
const is_overnight = (openhr) => openhr.is_overnight

export default {
	OpenHours: {
		day,
		start,
		end,
		is_overnight
	}
}