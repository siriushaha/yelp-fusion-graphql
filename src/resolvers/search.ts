const region = (result) => result.region
const total = (result) => result.total
const businesses = (result) => result.businesses
const reviews = (result) => result.reviews

export default {
  SearchResult: {
    region,
    total,
    businesses
  },
  PhoneSearchResult: {
    total,
    businesses
  },
  ReviewResult: {
    total,
    reviews
  }
}

