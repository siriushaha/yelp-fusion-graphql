const latitude = (coordinate) => coordinate.latitude
const longitude = (coordinate) => coordinate.longitude

export default {
  Coordinate: {
    latitude,
    longitude
  }
}
