const text = (review) => review.text
const url = (review) => review.url
const rating = (review) => review.rating
const time_created = (review) => review.time_created
const user = (review) => review.user

export default {
	Review:  {
	  text,
	  url,
	  rating,
	  time_created,
	  user
	}
}
