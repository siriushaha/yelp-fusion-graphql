const name = (user) => user.name
const image_url = (user) => user.image_url

export default {
	User: {
	  name,
	  image_url
	}
}
