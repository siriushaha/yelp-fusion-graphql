
const alias = (category) => category.alias
const title = (category) => category.title

export default {
  Category: {
    alias,
    title
  },
}
