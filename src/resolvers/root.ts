const searchForBusiness = (_, params, context) => context.yelpApi.searchForBusiness(params.location, params.term, params.sortby, params.limit)
const phoneSearchForBusiness = (_, params, context) => context.yelpApi.phoneSearchForBusiness(params.phone)
const transactionSearch = (_, params, context) => context.yelpApi.transactionSearch(params.location)
const getBusiness = (_, params, context) => context.yelpApi.getBusiness(params.businessId)
const getReviews = (_, params, context) => context.yelpApi.getReviews(params.businessId)
const autoComplete = (_, params, context) => context.yelpApi.autoComplete(params.text, params.longitude, params.latitude)

export default {
  RootQuery: {
    searchForBusiness,
    phoneSearchForBusiness,
    transactionSearch,
    getBusiness,
    getReviews,
    autoComplete
  },
}
