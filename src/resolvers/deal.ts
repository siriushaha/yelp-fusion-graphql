const dotitle = (dealOption) => dealOption.title
const purchase_url = (dealOption) => dealOption.purchase_url
const price = (dealOption) => dealOption.price
const formatted_price = (dealOption) => dealOption.formatted_price
const original_price = (dealOption) => dealOption.original_price
const formatted_original_price = (dealOption) => dealOption.formatted_original_price
const is_quantity_limited = (dealOption) => dealOption.is_quantity_limited
const remaining_count = (dealOption) => dealOption.remaining_count

const id = (deal) => deal.id
const dtitle = (deal) => deal.title
const url = (deal) => deal.url
const image_url = (deal) => deal.image_url
const currency_code = (deal) => deal.currency_code
const time_start = (deal) => deal.time_start
const time_end = (deal) => deal.time_end
const is_popular = (deal) => deal.is_popular
const what_you_get = (deal) => deal.what_you_get
const important_restrictions = (deal) => deal.important_restrictions
const additional_restrictions = (deal) => deal.additional_restrictions
const options = (deal) => deal.options

export default {
  DealOption: {
    title: dotitle,
    purchase_url,
    price,
    formatted_price,
    original_price,
    formatted_original_price,
    is_quantity_limited,
    remaining_count
  },
  Deal: {
    id,
    title: dtitle,
    url,
    image_url,
    currency_code,
    time_start,
    time_end,
    is_popular,
    what_you_get,
    important_restrictions,
    additional_restrictions,
    options
  }
}
