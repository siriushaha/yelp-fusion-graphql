const terms = (result) => result.terms
const businesses = (result) => result.businesses
const categories = (result) => result.categories

export default {
	AutoCompleteResult: {
	  terms,
	  businesses,
	  categories
	}
}
