const price = (giftCertificateOption) => giftCertificateOption.price
const formatted_price = (giftCertificateOption) => giftCertificateOption.formatted_price

const id = (giftCertificate) => giftCertificate.id
const url = (giftCertificate) => giftCertificate.url
const image_url = (giftCertificate) => giftCertificate.image_url
const currency_code = (giftCertificate) => giftCertificate.currency_code
const unused_balances = (giftCertificate) => giftCertificate.unused_balances
const options = (giftCertificate) => giftCertificate.options

export default {
  GiftCertificateOption: {
    price,
    formatted_price
  },
  GiftCertificate: {
    id,
    url,
    image_url,
    currency_code,
    unused_balances,
    options
  }  
}
