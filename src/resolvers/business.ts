const id = (business) => business.id
const name = (business) => business.name
const image_url = (business) => business.image_url
const is_closed = (business) => business.is_closed
const url = (business) => business.url
const price = (business) => business.price
const phone = (business) => business.display_phone
const rating = (business) => business.rating
const review_count = (business) => business.review_count
const distance = (business) => business.distance * 0.000621371
const categories = (business) => business.categories
const coordinates = (business) => business.coordinates
const location = (business) => business.location

const is_claimed = (bizinfo) => bizinfo.is_claimed
const photos = (bizinfo) => bizinfo.photos
const hours = (bizinfo) => bizinfo.hours

export default {
  Business: {
    id,
    name,
    image_url,
    is_closed,
    url,
    price,
    phone,
    rating,
    review_count,
    distance,
    categories,
    coordinates,
    location
  },
  BusinessInfo:  {
    id,
    name,
    image_url,
    is_claimed,
    is_closed,
    url,
    price,
    phone,
    rating,
    review_count,
    photos,
    hours,
    categories,
    coordinates,
    location
  }
}
