const center = (region) => region.center

export default {
  Region: {
    center
  }

}
