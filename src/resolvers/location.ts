
const address1 = (location) => location.address1
const address2 = (location) => location.address2
const address3 = (location) => location.address3
const city = (location) => location.city
const state = (location) => location.state
const zip_code = (location) => location.zip_code
const country = (location) => location.country
const display_address = (location) => location.display_address
const cross_streets = (location) => location.cross_streets

export default {
  Location: {
	  address1,
	  address2,
	  address3,
	  city,
	  state,
	  zip_code,
	  country,
	  display_address,
	  cross_streets
  }
}
