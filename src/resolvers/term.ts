const text = (term) => term.text

export default {
	Term: {
		text
	}
}
