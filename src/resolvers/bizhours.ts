const is_open_now = (bizhr) => bizhr.is_open_now
const hours_type = (bizhr) => bizhr.hours_type
const open = (bizhr) => bizhr.open

export default {
	BusinessHours: {
	  is_open_now,
	  hours_type,
	  open
	}
}