import root from './root'
import search from './search'
import region from './region'
import category from './category'
import coordinate from './coordinate'
import location from './location'
import business from './business'
import openhours from './openhours'
import bizhours from './bizhours'
import bizinfo from './bizinfo'
import review from './review'
import autocomplete from './autocomplete'

const schema = `
  schema {
    query: RootQuery
  }
`

export default [
  region,
  category,
  coordinate,
  location,
  business,
  openhours,
  bizhours,
  bizinfo,
  review,
  search,
  autocomplete,
  root,
  schema
]
