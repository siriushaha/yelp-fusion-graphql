export default `

type OpenHours  {
	day: Int,
	start: String,
	end: String,
	is_overnight: Boolean
}

`