export default `

type GiftCertificateOption {
  price: Float,
  formatted_price: String
}

type GiftCertificate {
  id: String,
  url: String,
  image_url: String,
  currency_code: String,
  unused_balances: String,
  options: [GiftCertificateOption]
}  
`