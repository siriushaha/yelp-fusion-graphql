export default `

type Coordinate {
  latitude: Float,
  longitude: Float
}

`
