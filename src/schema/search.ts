export default `

type SearchResult {
  region: Region,
  total: Int,
  businesses: [Business]
}

type PhoneSearchResult {
  total: Int,
  businesses: [Business]
}

type ReviewResult {
  total: Int,
  reviews: [Review]
}

`