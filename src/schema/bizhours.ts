export default `

type BusinessHours  {
  is_open_now: Boolean,
  hours_type: String,
  open: [OpenHours]
}

`