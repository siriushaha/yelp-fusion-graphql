export default `

type DealOption {
      title: String,
      purchase_url: String,
      price: Float,
      formatted_price: String,
      original_price: Float,
      formatted_original_price: String,
      is_quantity_limited: Boolean,
      remaining_count: Int
}


type Deal {
      id: String,
      title: String,
      url: String,
      image_url: String,
      currency_code: String,
      time_start: Int,
      time_end: Int,
      is_popular: Boolean,
      what_you_get: String,
      important_restrictions: String,
      additional_restrictions: String,
      options: [DealOption]
}


`