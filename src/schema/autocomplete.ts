export default `

type Term {
	text: String
}

type AutoCompleteResult {
  terms: [Term],
  businesses: [Business],
  categories: [Category]
}

`
