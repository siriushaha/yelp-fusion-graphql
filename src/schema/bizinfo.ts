export default `

type BusinessInfo  {
  id: String,
  name: String,
  image_url: String,
  is_claimed: Boolean,
  is_closed: Boolean,
  url: String,
  price: String,
  phone: String,
  rating: Float,
  review_count: Int,
  photos: [String],
  hours: [BusinessHours],
  categories: [Category],
  coordinates: Coordinate,
  location: Location
}

`