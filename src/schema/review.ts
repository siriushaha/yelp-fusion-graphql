export default `


type User {
  name: String,
  image_url: String
}

type Review {
  text: String,
  url: String,
  rating: Float,
  time_created: String,
  user: User
}

`
