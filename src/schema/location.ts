export default `

type Location {
  address1: String,
  address2: String,
  address3: String,
  city: String,
  state: String,
  zip_code: String,
  country: String,
  display_address: [String],
  cross_streets: String
}

`