export default `

type Business  {
  id: String,
  name: String,
  image_url: String,
  is_closed: Boolean,
  url: String,
  price: String,
  phone: String,
  rating: Float,
  review_count: Int,
  distance: Float,
  categories: [Category],
  coordinates: Coordinate,
  location: Location
}

`