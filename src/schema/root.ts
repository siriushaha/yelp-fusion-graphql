export default `

interface Node {
  id: ID!
}

type RootQuery {
  searchForBusiness(term: String, location: String, sortby: String, limit: Int): SearchResult
  phoneSearchForBusiness(phone: String): PhoneSearchResult
  transactionSearch(location: String): PhoneSearchResult
  getBusiness(businessId: String): BusinessInfo
  getReviews(businessId: String): ReviewResult
  autoComplete(text: String, longitude: Float, latitude: Float): AutoCompleteResult
}


`
