import BaseModel from './base';

export default class YelpApi extends BaseModel {

  public searchForBusiness(location: string, term: string, sortby: string, limit?: number) {
    return this.connector.search(term, location, sortby, limit);
  }

  public phoneSearchForBusiness(phone: string) {
    return this.connector.phoneSearch(phone);
  }
  
  public transactionSearch(location: string) {
    return this.connector.transactionSearch(location);
  }
  
  public getBusiness(businessId: string) {
    return this.connector.getBusiness(businessId);
  }
  
  public getReviews(businessId: string) {
    return this.connector.getReviews(businessId);
  }
  
  public autoComplete(text: string, longitude: number, latitude: string) {
    return this.connector.autoComplete(text, longitude, latitude);
  }

}
