import YelpApiConnector from '../connectors/yelpapi'

export default class BaseModel {
  protected connector: YelpApiConnector

  constructor(connector) {
    this.connector = connector
  }

}
