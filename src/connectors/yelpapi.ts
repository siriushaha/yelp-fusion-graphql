'use strict';

import Yelp  from './yelpconnector';

const yelpAPIKeys = '-Z69abkze2X-ggLoQ_rJr5VpSoY9CIWbSkJFmkH6SylLO-6O4fvcqDOT7AxniWQFd9KCfWqQMdBfmgkLIfL4WHUyxjZfQM2NoEzZAkq3GMQosqXMWLMhs9weKSrqWnYx';

export default class YelpApiConnector {

  private yelp: any

  constructor() {
    this.yelp = new Yelp({
      app_secret: yelpAPIKeys
    })
  }

  public search(term: string, location: string, sort_by: string, limit: number) {
    const searchRequest = { term, location, sort_by, limit }
    return new Promise<any>((resolve, reject) => {
      this.yelp.search(searchRequest)
          .then( (data) => {
            let result = JSON.parse(data);
            //console.log(result);
            resolve(result);
          })
          .catch( (err) => {
            console.error(err);
            reject(err);
          });
    })
  }

  public phoneSearch(phone: string) {
    const searchRequest = { phone }
    return new Promise<any>((resolve, reject) => {
      this.yelp.phoneSearch(searchRequest)
          .then( (data) => {
            let result = JSON.parse(data);
            //console.log(result);
            resolve(result);
          })
          .catch( (err) => {
            console.error(err);
            reject(err);
          });
    })
  }

  public transactionSearch(location: string) {
    const searchRequest = { location }
    return new Promise<any>((resolve, reject) => {
      this.yelp.transactionSearch('delivery',searchRequest)
          .then( (data) => {
            let result = JSON.parse(data);
            //console.log(result);
            resolve(result);
          })
          .catch( (err) => {
            console.error(err);
            reject(err);
          });
    })
  }

  public getBusiness(businessId: string) {
    return new Promise<any>((resolve, reject) => {
      this.yelp.business(businessId)
          .then( (data) => {
            let result = JSON.parse(data);
            //console.log(result);
            resolve(result);
          })
          .catch( (err) => {
            console.error(err);
            reject(err);
          });
    })
  }

  public getReviews(businessId: string) {
    return new Promise<any>((resolve, reject) => {
      this.yelp.reviews(businessId)
          .then( (data) => {
            let result = JSON.parse(data);
            //console.log(result);
            resolve(result);
          })
          .catch( (err) => {
            console.error(err);
            reject(err);
          });
    })
  }

  public autoComplete(text: string, longitude: number, latitude: string) {
    const searchRequest = { text, longitude, latitude }
    return new Promise<any>((resolve, reject) => {
      this.yelp.autocomplete(searchRequest)
          .then( (data) => {
            let result = JSON.parse(data);
            //console.log(result);
            resolve(result);
          })
          .catch( (err) => {
            console.error(err);
            reject(err);
          });
    })
  }

}
