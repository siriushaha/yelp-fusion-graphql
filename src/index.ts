import { makeExecutableSchema } from 'graphql-tools'

import typeDefs from './schema/index'
import resolvers from './resolvers/index'

import YelpApiConnector from './connectors/yelpapi'

import YelpApi from './models/yelpapi'

import { startExpress } from './express'

const yelpApiHost = process.env.SDS_API_HOST ? `${process.env.SDS_API_HOST}` : 'https://api.yelp.com'

const schema = makeExecutableSchema({ typeDefs, resolvers })

function graphqlOptions() {
  const yelpApiConnector = new YelpApiConnector()

  return {
      pretty: true,
      schema,
      context: {
          yelpApi: new YelpApi(yelpApiConnector)
      }
  }
}

startExpress(graphqlOptions)

