import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as apollo from 'apollo-server'


const expressPort = process.env.EXPRESS_PORT || 8082
const app = express()
const cors = require('cors')

export function startExpress(graphqlOptions) {
/*
  let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    //res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
  };
  app.use(allowCrossDomain);
  */
  app.use('*', cors());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json())
  app.use('/graphql', apollo.apolloExpress(graphqlOptions))
  app.use('/', apollo.graphiqlExpress({endpointURL: '/graphql'}))

  app.listen(expressPort, () => {
      console.log(`GraphQL server for Yelp Fusion Search API v3 listening on ${expressPort}`)
  })
}

